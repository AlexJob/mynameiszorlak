# README #

O meu nome é Zorlak o bot


### Install process ###

* Install nodejs
* [Install ffmpeg](https://ffmpeg.org/download.html#build-linux)

### How to run ###

* Open terminal in the project root
* run nodeJs => `nodejs .`
* To run as a service copy the zorlakbot.service file to /usr/systemd/system/
* run with systemctl start zorlakbot

### To add new commands ###

* Open `commands.txt`
* add new commands and the path to their audio, following the structure:
    * `!command:./myFilePath`