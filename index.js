#!/usr/bin/env node
const { Console } = require('console');
const Discord = require('discord.js');
const auth = require('./auth.json');
const Fs = require('fs');  
const Path = require('path');

//music channel
const allowedChannels = ['259134819382853632'];
const commandsFilePath = './commands.txt';

let bot = new Discord.Client();
let isReady = true;
let commandsMap = {};
let cmdsString = "```Queres ouvir o Zorlak a falar?\n\nCommands\n\n";
let voiceChannel = undefined;

if(isFileInPath(commandsFilePath)){
  try {
    // read contents of the file
    const data = Fs.readFileSync(commandsFilePath, 'UTF-8');

    // split the contents by new line
    const lines = data.split(/\r?\n/);

    lines.forEach(function(line, idx, array){
      let cmdArray = line.split(':');
      //adds content to Map
      commandsMap[cmdArray[0]] = cmdArray[1];

      //adds content to cmdsString
      cmdsString = cmdsString + cmdArray[0] + '\n';
      
      if (idx === array.length - 1){ 
        cmdsString = cmdsString + '```';
      }
      
    });
  } catch (err) {
      console.error(err);
  }
}

bot.once('ready', () => {
  console.log('Zorlak is online!');
});

bot.on('message', message => {
  if (isReady && isChannelForCommands(message.channel.id) && isMessageCommand(message.content)) {
    
    if(message.content === '!cmds'){
      message.channel.send(cmdsString);
    }

    //checks if command exists in Map
    if((commandsMap[message.content] != undefined) && !(message.content === "!leave")){
      playAudio(message, commandsMap[message.content]);
    }
    else if (message.content === "!leave"){
      disconnectChannel();
    }
  }
});

bot.login(auth.token);

function playAudio(message, audioPath){
  isReady = false;
  if ((this.voiceChannel === undefined) || (this.voiceChannel != message.member.voice.channel)) {
    setVoiceChannel(message.member.voice.channel);
  }

  this.voiceChannel.join().then(connection => {
    const dispatcher = connection.play(require("path").join(__dirname, audioPath));
    dispatcher.on('finish', end => {
      console.log("### audio ended");
      disconnectChannel();
    });
  }).catch(err => console.log(err)).then(isReady = true);
}

function disconnectChannel(){
  if (this.voiceChannel != undefined){
    this.voiceChannel.leave();
    isReady = true;
  }
}

function setVoiceChannel(voiceChannel){
  this.voiceChannel = voiceChannel;
}

function isFileInPath(pathString){
  const path = Path.join(__dirname, pathString);
  return Fs.existsSync(path);
}

function isChannelForCommands(channelId) {
  return allowedChannels.includes(channelId);
}

function isMessageCommand(message){
  return message.charAt(0) == '!' ? true : false;
}